#!/bin/bash

# From shell
#docker run --name evan-rasa -u 1000:1000 -p 5005:5005 -it -v $(pwd)/rasa-docker-compose:/app rasa/rasa:3.4.2-full shell

# From api
docker run --name evan-rasa-1 -u 1000:1000 -p 5005:5005 -it -v $(pwd)/rasa-docker-compose:/app rasa/rasa:3.4.2-full run -m models --enable-api --cors "*" --debug
